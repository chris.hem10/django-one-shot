from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView


from todos.models import TodoList, TodoItem

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todo_lists/list.html"
    context_object_name = "todolists"


# Use this function to check your context name in CBV
# def get_context_data(self, *args, **kwargs):
#     context = super().get_context_data(*args, **kwargs)
#     print(context)
#     return context


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todo_lists/detail.html"


class TodoListCreate(CreateView):
    model = TodoList
    template_name = "todo_lists/new.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListUpdate(UpdateView):
    model = TodoList
    template_name = "todo_lists/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDelete(DeleteView):
    model = TodoList
    template_name = "todo_lists/delete.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_list")


class TodoItemCreate(CreateView):
    model = TodoItem
    template_name = "todo_items/new.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdate(UpdateView):
    model = TodoItem
    template_name = "todo_items/update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


# def show_todolists(request):
#     # Get all the instances of Todolist
#     todolists = TodoList.objects.all()
#     print(todolists)
#     # create a context
#     # Render a template
#     return render(request, "todos/list.html", {"todolists": todolists})


# Use this function to check your context name in CBV
# def get_context_data(self, *args, **kwargs):
#     context = super().get_context_data(*args, **kwargs)
#     print(context)
#     return context


# Detail View FBV
# def show_todolist(request, pk):
#     print(pk)
#     # Get a particular todolist
#     todolist = TodoList.objects.get(pk=pk)
#     print(todolist)
#     # Make a context
#     # Render a template
#     return render(request, "todos/detail.html", {"todolist": todolist})


# def create_todolist(request):
#     if request.method == "POST":
#         form = TodoListForm(request.POST)
#         if form.is_valid():
#             todolist = form.save(commit=False)
#             return redirect("todos/detail.html", pk=todo.pk)
#     else:
#         form = TodoListForm()
#     return render(
#         request,
#         "todos/new.html",
#         {"form": form}
#     )
