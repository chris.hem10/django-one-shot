from django.urls import path

from todos.views import (
    TodoListView,
    TodoDetailView,
    TodoListCreate,
    TodoListUpdate,
    TodoListDelete,
    TodoItemCreate,
    TodoItemUpdate,
    # show_todolists,
    # show_todolist,
    # create_todolist,
)

urlpatterns = [
    path("", TodoListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoDetailView.as_view(), name="todo_list_detail"),
    path("create/", TodoListCreate.as_view(), name="todo_list_create"),
    path(
        "<int:pk>/edit/",
        TodoListUpdate.as_view(),
        name="todo_list_update",
    ),
    path(
        "<int:pk>/delete/",
        TodoListDelete.as_view(),
        name="todo_list_delete",
    ),
    path(
        "items/create/",
        TodoItemCreate.as_view(),
        name="todo_item_create",
    ),
    path(
        "items/<int:pk>/edit/",
        TodoItemUpdate.as_view(),
        name="todo_item_update",
    ),
    # path("", show_todolists, name="todo_list_list"),
    # /todos/1 << number comes from the URL
    # path("<int:pk>/", show_todolist, name="todo_list_detail"),
    # path("create/", create_todolist, name="todo_list_create"),
]
